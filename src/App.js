import React, { useState } from 'react';
import { Text, TextInput, View, Image, Button } from 'react-native';

const getFullName = (firstName, secondName) => {
    return secondName + " " + firstName;
}

const Cat = (props) => {
    return (
        <Text>Hello, I am {getFullName(props.name, "Lechat")} !</Text>
    );
}

const InputCat = () => {
    return (
        <View>
            <Text>Hello, I am the new cat, I am...</Text>
            <TextInput
                style={{
                    height: 40,
                    borderColor: 'gray',
                    borderWidth: 1
                }}
                defaultValue="Name this cat!"
            />
        </View>
    );
}

const Many = () => {
    return (
        <View>
            <Text>Welcome !!!</Text>
            <Cat name="Malo "/>
            <Cat name="Badabada"/>
            <Image
                source={{uri: "https://reactnative.dev/docs/assets/p_cat1.png"}}
                style={{width: 200, height: 200}}
            />
            <InputCat />
        </View>
    )
}

const Cafe = () => {
    const [isFed, setIsFed] = useState(true);
    return (
        // <></> egal à <View></View>
        <>
            <Many />
            <Text>
                The cats were {isFed ? "not fed" : "fed"}!
            </Text>
            <Button
                onPress={() => {
                    setIsFed(false);
                }}
                disabled={!isFed}
                title={isFed ? "Pour them some milk, please!" : "Thank you!"}
            />
        </>
    )
}
export default Cafe;
