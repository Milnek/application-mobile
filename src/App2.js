import React, { useState } from 'react';
import { Text, TextInput, View, Image, Button } from 'react-native';

const Cat = (props) => {
    return (
        <Text>Hello, I am Lechat {props.name} !</Text>
    );
}

const InputCat = () => {
    let name = "..";
    return (
        <>
            <Text>Hello, I am the new cat, I am {name}.</Text>
            <TextInput
                style={{
                    height: 40,
                    borderColor: 'gray',
                    borderWidth: 1
                }}
                defaultValue="Name this cat!"
            />
            <Button
                onPress={() => {name = "textinputvalue";}}
                disabled=true
                title="set the name"
            />
        </>
    );
}

const Many = () => {
    return (
        <View>
            <Text>Welcome !!!</Text>
            <Cat name="Malo"/>
            <Cat name="Badabada"/>
            <Image
                source={{uri: "https://reactnative.dev/docs/assets/p_cat1.png"}}
                style={{width: 200, height: 200}}
            />
            <InputCat />
        </View>
    )
}


export default InputCat;
